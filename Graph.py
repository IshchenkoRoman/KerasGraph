from keras.models import *
from keras.layers import *
from keras.optimizers import *
from keras.datasets import cifar10
from keras import backend as K
from os import environ

from subprocess import call

os.environ['KERAS_BACKEND'] = 'tensorflow'

def saveModel(model, name="KerasCNN.h5"):

    model.save(name)

def trainModel(model=None):
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    batch_range = x_train.shape[0] // 1000
    # model = load_model('KerasCNN.h5')
    model.compile(loss=["sparse_categorical_crossentropy"], optimizer=Adam(0.001))
    model.fit(x=x_train, y=y_train, batch_size=1024, epochs=10, verbose=1)

    saveModel(model)

    return model

def Graph():
    learning_rate = 0.001

    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    batch, shape_xx, shape_xy, shape_xz = x_train.shape
    # batch, shape_y  = y_train.shape
    input_x = Input(shape=(shape_xx, shape_xy, shape_xz))
    # input_y = Input(shape=(shape_y,))

    x_image = Reshape((shape_xx, shape_xy, shape_xz))(input_x)
    print(x_image._keras_shape)
    # First Layer

    h_conv1 = Activation('relu')(Conv2D(32, 2, padding="same")(x_image))
    h_conv1 = AveragePooling2D(pool_size=(2,2), strides=(2,2), padding="same")(h_conv1)

    #Second Layer

    h_conv2 = Activation("relu")(Conv2D(64, 5, padding="same", strides=2)(h_conv1))
    h_conv2 = AveragePooling2D(pool_size=(2, 2), strides=(2, 2), padding="same")(h_conv2)
    #Third Layer

    h_conv2_flat = Flatten()(h_conv2)
    h_fc1 = Dropout(0.3)(Activation("relu")(Dense(1024)(h_conv2_flat)))
    h_fc2 = Dropout(0.3)(Activation("relu")(Dense(10)(h_fc1)))

    y_CNN = Dense(batch, activation="softmax", name="y_CNN")(h_fc2)
    print(y_CNN.get_shape())

    model = Model(inputs=input_x, outputs=y_CNN)

    model.compile(loss=["sparse_categorical_crossentropy"], optimizer=Adam(learning_rate))
    return model

def optimizeGraph():
    call(["sh", "OptimizeFozen.sh"])

def main():
    # print(K.backend())
    # model = Graph()
    # # saveModel(model)
    # model = trainModel(model)



if __name__ == "__main__":
    main()