python -m tensorflow.python.tools.optimize_for_inference \
  --input=SimpleGraph.pb \
  --input_binary=True \
  --frozen_graph=True \
  --output=Optimize_graph.pb\
  --output_names=res \
  --input_names=prefix/a_1 prefix/b_1