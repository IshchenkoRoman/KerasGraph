import keras
from keras.models import *
from keras.layers import *
from keras.optimizers import *
from keras.datasets import cifar10

from keras import backend as K

from tensorflow.python.tools import freeze_graph
from tensorflow.python.tools import optimize_for_inference_lib

from subprocess import call

def saveGraph(model, name="SimpleGraph.h5"):

    model.save(name)

def convert_to_pb(weight_file="SimpleGraph.h5",input_fld='',output_fld=''):

    import os
    import os.path as osp
    from tensorflow.python.framework import graph_util
    from tensorflow.python.framework import graph_io
    from keras.models import load_model
    from keras import backend as K


    # weight_file is a .h5 keras model file
    output_node_names_of_input_network = ['a', 'b']
    output_node_names_of_final_network = 'output_node'

    # change filename to a .pb tensorflow file
    output_graph_name = weight_file[:-2]+'pb'
    weight_file_path = osp.join(input_fld, weight_file)

    net_model = load_model(weight_file_path)

    num_output = len(output_node_names_of_input_network)
    pred = [None]*num_output
    pred_node_names = [None]*num_output

    for i in range(num_output):
        pred_node_names[i] = output_node_names_of_final_network+str(i)
        pred[i] = tf.identity(net_model.output[i], name=pred_node_names[i])

    sess = K.get_session()

    constant_graph = graph_util.convert_variables_to_constants(sess, sess.graph.as_graph_def(), pred_node_names)
    graph_io.write_graph(constant_graph, output_fld, output_graph_name, as_text=False)
    print('saved the constant graph (ready for inference) at: ', osp.join(output_fld, output_graph_name))

    return output_fld+output_graph_name

def load_graph(frozen_graph_filename="SimpleGraph.pb"):
    # We load the protobuf file from the disk and parse it to retrieve the
    # unserialized graph_def
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    a1 = np.ones((10, 10)).reshape(1, 10, 10)
    b1 = np.zeros((10, 10)).reshape(1, 10, 10)

    # Then, we can use again a convenient built-in function to import a graph_def into the
    # current default Graph
    with tf.Graph().as_default() as graph:
        tf.import_graph_def(
            graph_def,
            name="prefix",
        )

    # input_name = graph.get_operations()[0].name+':0'
    # output_name = graph.get_operations()[-1].name+':0'



    output_name = tf.get_default_graph().get_tensor_by_name("res/add:0")

    # input_name = (input_a, input_b)

    # print([tensor.name for tensor in tf.get_default_graph().as_graph_def().node])
    print([t.name for op in tf.get_default_graph().get_operations() for t in op.values()])

    with tf.Session(graph=graph) as sess:
        input_a = tf.get_default_graph().get_tensor_by_name("prefix/a_1:0")
        input_b = tf.get_default_graph().get_tensor_by_name("prefix/b_1:0")
        # output_name = tf.get_default_graph().get_tensor_by_name("prefix/output_node1:0")
        print([tensor.name for tensor in tf.get_default_graph().as_graph_def().node])
        res = sess.run(output_name, feed_dict={input_a: a1, input_b: b1})

    return graph, output_name

def buildGraph():

    a = Input(shape=(10, 10), name="a")
    b = Input(shape=(10, 10), name="b")

    a1 = Reshape((10, 10), name="a1")(a)
    b1 = Reshape((10, 10), name="b1")(b)

    res = add([a1, b1], name="res")
    # res = dot([a1, b1], axes=1)

    model = Model(inputs=[a, b], outputs=res)

    saveGraph(model)

    return model

def optimizeGraph():
    call(["sh", "OptimizeFozen.sh"])

def predict(model_path="SimpleGraph.pb", input_data=None):
    # load tf graph
    tf_model,tf_input,tf_output = load_graph(model_path)

    a, b = tf_input

    print("\n\n", a, b)

    # Create tensors for model input and output
    # x = tf_model.get_tensor_by_name(tf_input)
    # y = tf_model.get_tensor_by_name(tf_output)

    a1 = np.ones((10, 10)).reshape(1, 10, 10)
    b2 = np.zeros((10, 10)).reshape(1, 10, 10)

    # Number of model outputs
    with tf.Session(graph=tf_model) as sess:
        print([tensor.name for tensor in tf.get_default_graph().as_graph_def().node])
        res = sess.run(tf_output, feed_dict={"prefix/a_1:0": a1, "prefix/b_1:0": b2})
        # y_out = sess.run(y, feed_dict={x: input_data[i:i+1]})


def main():

    model = buildGraph()
    print(model.summary())
    load_graph()
    #
    # a = np.ones((10, 10)).reshape(1, 10, 10)
    # # b = np.zeros((10, 10)).reshape(1, 10, 10)
    # b = np.ones((10, 10)).reshape(1, 10, 10) * 2
    #
    # print(a.shape)
    #
    # res = model.predict([a, b], batch_size=1)
    # convert_to_pb()
    # print(res)

    # optimizeGrasph()
    # predict()s


if __name__ == "__main__":
    main()